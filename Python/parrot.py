from abc import ABC, abstractproperty

class Parrot(ABC):
    base_speed = 12.0

    @abstractproperty
    def speed(self):
        pass

class EuropeanParrot(Parrot):
    @property
    def speed(self) -> float:
        return self.base_speed


class AfricanParrot(Parrot):
    load_factor = 9.0

    def __init__(self, *, coconuts: int):
        self.coconuts = coconuts

    @property
    def speed(self) -> float:
        return max(0, self.base_speed - self.load_factor * self.coconuts)


class NorwegianBlue(Parrot):
    def __init__(self, *,  voltage: float, nailed: bool):
        self.voltage = voltage
        self.nailed = nailed

    @property
    def speed(self) -> float:
        return 0 if self.nailed else min(24.0, self.voltage * self.base_speed)
