from parrot import EuropeanParrot, AfricanParrot, NorwegianBlue


def test_speedOfEuropeanParrot():
    parrot = EuropeanParrot()
    assert parrot.speed == 12.0


def test_speedOfAfricanParrot_With_One_Coconut():
    parrot = AfricanParrot(coconuts=1)
    assert parrot.speed == 3.0


def test_speedOfAfricanParrot_With_Two_Coconuts():
    parrot = AfricanParrot(coconuts=2)
    assert parrot.speed == 0.0


def test_speedOfAfricanParrot_With_No_Coconuts():
    parrot = AfricanParrot(coconuts=0)
    assert parrot.speed == 12.0


def test_speedNorwegianBlueParrot_nailed():
    parrot = NorwegianBlue(voltage=1.5, nailed=True)
    assert parrot.speed == 0.0


def test_speedNorwegianBlueParrot_not_nailed():
    parrot = NorwegianBlue(voltage=1.5, nailed=False)
    assert parrot.speed == 18.0


def test_speedNorwegianBlueParrot_not_nailed_high_voltage():
    parrot = NorwegianBlue(voltage=4, nailed=False)
    assert parrot.speed == 24.0
